<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class ProductController extends Controller
{

    public function index() : View
    {
        $products = Product::all();
        $categories = Category::all();
        return view('productsView', compact('products', 'categories'));
        return view('productsView')->with('products',$products);
    }


    public function delete(Request $request, Product $product) {
        $product->delete();
        return redirect('products');
    }


    public function warn(Request $request, Product $product) {
        return view('products-warn')->with('product', $product);
    }

    public function insert(Request $request) {
        return view('products-insert');
    }


    public function create(Request $request) {
        $request->validate([
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:110',
            'price' => 'required|integer|max:1000',
            'quantity' => 'required|integer|max:150'
        ]);
        $products = new Product();
        $products->name = $request->input('name');
        $products->description = $request->input('description');
        $products->price = $request->input('price');
        $products->quantity = $request->input('quantity');
        //$products->display_order = $products->id;
        $products->save();
        return redirect('products');
    }


    public function confirmDelete($id)
    {
        $product = Product::find($id);
        return view('products.confirmation', compact('product'));
    }

    public function storeRating(Request $request, Product $product)
    {
        $request->validate([
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string|max:255',
        ]);

        $rating = new Rating([
            'user_id' => auth()->id(),
            'product_id' => $product->id,
            'rating' => $request->rating,
            'comment' => $request->comment,
        ]);

        $rating->save();

        return redirect()->back()->with('success', 'Hecha');
    }

    public function show(Product $product): View
    {
        return view('productDetails', compact('product'));
    }

    public function assignCategory(Request $request, Product $product)
    {
        $request->validate([
            'category' => 'required|exists:categories,id',
        ]);

        $product->category_id = $request->category;
        $product->save();

        return redirect()->route('productsView')->with('success', 'Asignada');
    }



}
