<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function indexInvoice(Request $request) {
        $products = Product::all();
        $clients = Client::all();
        $orders = Invoice::with('client', 'products')->get();

        return view('invoicesView')->with([
            'clients' => $clients,
            'products' => $products,
            'orders' => $orders
        ]);
    }

    public function showInvoices()
    {
        $orders = Invoice::with('client', 'products')->get();
        return view('InvoicesView', compact('orders'));
    }

    public function createInvoice(Request $request) {
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'price' => 'required|integer|max:99999',
            'quantity' => 'required|integer|max:255'
        ]);
        DB::transaction(function () use ($request) {
            $order = Invoice::create([
                'client_id' => $request->input('client_id'),
                'order_date' => now(),
            ]);

            foreach ($request->input('products') as $productData) {
                $product = Product::findOrFail($productData['id']);

                $product->quantity -= $productData['quantity'];
                $product->save();

                $order->products()->attach($product->id, [
                    'quantity' => $productData['quantity'],
                    'price' => $productData['price'],
                ]);

            }
        });

        return redirect()->route('invoices.index')->with('success', 'Creada');
    }
}
