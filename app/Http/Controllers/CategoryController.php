<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function indexCategory()
    {
        $Category = Category::all();
        return view('categoryView')->with('categories',$Category);
    }
    public function CategoryCreate(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|max:50',
            'percent'=>'required'
        ]);

        $Category = new Category();
        $Category->name = $request->input('name');
        $Category->percent = $request->input('percent');
        $Category->save();
        return redirect('/categories');
    }
    public function deleteCategory(Category $category)
    {
        $category->delete();
        return redirect('/categories');
    }




    public function showForm(){
        return view('CategoryAdd');
    }

}
