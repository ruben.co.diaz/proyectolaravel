<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    function indexClient() {
        $clients = Client::all();
        return view('clientsView', ['clients' => $clients]);
    }

    public function deleteClient(Request $request, Client $client) {
        $client->delete();
        return redirect('clients');
    }
    public function warnClient(Request $request, Client $client) {
        return view('clients-warn')->with('client', $client);
    }



    public function insertClient(Request $request) {
        return view('clients-insert');
    }

    public function createClient(Request $request) {
        $request->validate([
            'name' => 'required|string|max:50',
            'address' => 'required|string|max:100',
            'age' => 'required|integer|max:200',
            'city' => 'required|string|max:50',
            'country' => 'required|string|max:50'
        ]);


        $client = new Client();
        $client->name = $request->input('name');
        $client->age = $request->input('age');
        $client->address = $request->input('address');
        $client->city = $request->input('city');
        $client->country = $request->input('country');
        $client->save();
        return redirect('clients');
    }
}
