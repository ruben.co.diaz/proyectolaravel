<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'adress',
        'age',
        'city',
        'country'
    ];

    public function commands()
    {
        return $this->hasMany(Invoice::class);
    }

}
