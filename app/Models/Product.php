<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function products() : BelongsToMany
    {
        return $this->belongsToMany(Invoice::class);
    }

    protected $fillable = ['name', 'description', 'price', 'quantity', 'category_id'];

    public function commands()
    {
        return $this->belongsToMany(Invoice::class)->withPivot('quantity', 'price')->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }


}
