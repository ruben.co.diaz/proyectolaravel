<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('welcome');
})->name('inici');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/products', [ProductController::class, 'index'])->name('productsView');

Route::get('/products/{product}/delete', [ProductController::class, 'delete'])->name('products-delete');
Route::get('/product/{product}/warn', [ProductController::class, 'warn'])->name('products-warn');
// Recordatorio: Insertar o Crear
Route::get('/product/insert', [ProductController::class, 'insert'])->name('products-insert');
Route::post('/product/create', [ProductController::class, 'create'])->name('create');

Route::get('/products/{product}/delete/confirm', [ProductController::class, 'confirmDelete'])->name('products.confirmDelete');





// Route::post('/clients', [ClientController::class, 'indexClient'])->name('clients');

Route::get('/clients', [ClientController::class, 'indexClient'])->name('clientsView');

Route::get('/client/{client}/delete', [ClientController::class, 'deleteClient'])->name('clients-delete');

Route::get('/client/{client}/warn', [ClientController::class, 'warnClient'])->name('clients-warn');

Route::get('/client/insert', [ClientController::class, 'insertClient'])->name('clients-insert');

Route::post('/client/create', [ClientController::class, 'createClient'])->name('clients-create');






Route::get('/invoices', [InvoiceController::class, 'indexInvoice'])->name('invoicesView');
Route::post('/invoices-create', [InvoiceController::class, 'createInvoice'])->name('invoices-create');



Route::get('/categories',[CategoryController::class, 'indexCategory'])->name('CategoryView');
Route::get('/categories/{category}/delete',[CategoryController::class, 'deleteCategory'])->name('deleteCategory');
Route::post('/products/{product}/assign-category', [ProductController::class, 'assignCategory'])->name('putCategory');

Route::get('/formCategories',[CategoryController::class,'showForm'])->name('formCategories');
Route::post('/addCategory',[CategoryController::class, 'CategoryCreate'])->name('createCategory');



//hacer mostrar rating
Route::post('/products/{product}/rate', [ProductController::class, 'storeRating'])->name('products.storeRating');

Route::get('/products/{product}', [ProductController::class, 'show'])->name('products.show');



require __DIR__.'/auth.php';
