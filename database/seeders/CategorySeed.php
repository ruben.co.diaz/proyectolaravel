<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            ['name'=>'1 Categoria','percent'=>50],
            ['name'=>'2 Categoria','percent'=>10],
            ['name'=>'3 Categoria','percent'=>21],
        ]);
    }

}
