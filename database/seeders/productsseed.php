<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class productsseed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            ['name'=>'Small potion','price'=>74, 'description'=>'Description example'],
            ['name'=>'Bastard potion','price'=>27, 'description'=>'Description example'],
            ['name'=>'Mana potion','price'=>10, 'description'=>'Description example']
        ]);
    }

}
