<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
            ['name'=>'Ruben', 'age'=>19, 'address'=>'Santa Perpetua', 'city'=>'Barcelona', 'country'=>'Espanya','created_at'=>now(), 'updated_at'=>now()]
        ]);
    }
}
