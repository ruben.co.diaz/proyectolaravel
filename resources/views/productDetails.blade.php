<nav style="text-align: center; background-color: #ADD8E6;">
    <a href="{{ route('inici') }}">
        Home
    </a>
    <a href="{{ route('productsView') }}">
        Products
    </a>
    <a href="{{ route('clientsView') }}">
        Clients
    </a>
    <a href="{{ route('invoicesView') }}">
        Invoices
    </a>
    <a href="{{ route('CategoryView') }}">
        Categories
    </a>
</nav>
<div>
    <h2>{{ $product->name }}</h2>
    <p>Preu: {{ $product->price }}</p>
    <p>Descripció: {{ $product->description }}</p>
    <p>Quantitat: {{ $product->quantity }}</p>
</div>

<hr>

<form action="{{ route('products.storeRating', $product) }}" method="POST">
    @csrf
    <label for="rating">Puntuació:</label>
    <input type="number" name="rating" id="rating" min="1" max="5" required>
    <label for="comment">Comentari:</label>
    <textarea name="comment" id="comment" rows="3"></textarea>
    <button type="submit">Enviar Valoració</button>
</form>

@if($product->ratings->count() > 0)
    <div>
        <h3>Valoraciones</h3>
        @foreach($product->ratings as $rating)
            <p>Puntuación: {{ $rating->rating }}</p>
            <p>Comentario: {{ $rating->comment }}</p>
            <hr>
        @endforeach
    </div>
@endif
