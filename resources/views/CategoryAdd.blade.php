<nav style="text-align: center; background-color: #ADD8E6;">
    <a href="{{ route('inici') }}">
        Home
    </a>
    <a href="{{ route('productsView') }}">
        Products
    </a>
    <a href="{{ route('clientsView') }}">
        Clients
    </a>
    <a href="{{ route('invoicesView') }}">
        Invoices
    </a>
    <a href="{{ route('CategoryView') }}">
        Categories
    </a>
</nav>
<div>
    <div>
        <a class="enllaç" onclick="location.href='{{url('/products')}}'">Productes</a>
        <a class="enllaç" onclick="location.href='{{url('/categories')}}'">Categories</a>
        <a class="enllaç" onclick="location.href='{{url('/clients')}}'">Clients</a>
    </div>
</div>
<div>
    <h1>Afegir categoria:</h1>
    <form method="POST" action="{{route('createCategory')}}">
        @csrf
        <label>Nom: </label><input placeholder="name" name="name"><br><br>
        <label>IVA: </label><input placeholder="percent" name="percent"><br><br>
        <button type="submit">Afegir categoria</button>
    </form>
</div>
