<link rel="stylesheet" type="text/css" href="{{ url('/resources/css/menu.css') }}"/>

<nav style="text-align: center; background-color: #ADD8E6;">
    <a href="{{ route('inici') }}">
        Home
    </a>
    <a href="{{ route('productsView') }}">
        Products
    </a>
    <a href="{{ route('clientsView') }}">
        Clients
    </a>
    <a href="{{ route('invoicesView') }}">
        Invoices
    </a>
    <a href="{{ route('CategoryView') }}">
        Categories
    </a>
</nav>
<div>
    <table>
        <tr>
            <th>Nom</th>
            <th>Edat</th>
            <th>Adresa</th>
        </tr>
        @foreach ($clients as $client)
            <tr style="">
                <td>{{ $client->name }}</td>
                <td>{{ $client->age }}</td>
                <td>{{ $client->address }}, {{ $client->city }}, {{ $client->country }}</td>
                <td><button onclick="location.href='{{ url('/client/'.$client->id.'/warn') }}'">Borrar</button></td>
            </tr>
        @endforeach
    </table>
    <br>
    <div>
        <button onclick="location.href='{{ url('/client/insert') }}'">Nou client</button>
    </div>
</div>
