<link rel="stylesheet" type="text/css" href="{{ url('/resources/css/menu.css') }}" />
<nav style="text-align: center; background-color: #ADD8E6;">
    <a href="{{ route('inici') }}">
        Home
    </a>
    <a href="{{ route('productsView') }}">
        Products
    </a>
    <a href="{{ route('clientsView') }}">
        Clients
    </a>
    <a href="{{ route('invoicesView') }}">
        Invoices
    </a>
    <a href="{{ route('CategoryView') }}">
        Categories
    </a>
</nav>
<div>
    @foreach ($products as $product)
        <div>
            <div>{{ $product->name }}, {{ $product->price }}, {{ $product->description }}, {{ $product->quantity }}</div>

            <form action="{{ route('putCategory', $product) }}" method="POST">
                @csrf
                <label for="category">Categoría:</label>
                <select name="category" id="category">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </form>
            <button onclick="location.href='{{ url('/product/'.$product->id.'/warn') }}'">Borrar</button>
            <a href="{{ route('products.show', $product) }}">Veure detalls</a>
        </div>
    @endforeach
    <br>
    <div>
        <button onclick="location.href='{{ url('/product/insert') }}'">Crear</button>
    </div>

</div>

