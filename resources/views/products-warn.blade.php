<div>
    <p>Segur que vols borrar el client {{ $product->name }}?</p>
    <button onclick="location.href='{{ url('/products/'.$product->id.'/delete') }}'">Si</button>
    <button onclick="location.href='{{ url('/products') }}'">No</button>
</div>
