<nav style="text-align: center; background-color: #ADD8E6;">
    <a href="{{ route('inici') }}">
        Home
    </a>
    <a href="{{ route('productsView') }}">
        Products
    </a>
    <a href="{{ route('clientsView') }}">
        Clients
    </a>
    <a href="{{ route('invoicesView') }}">
        Invoices
    </a>
    <a href="{{ route('CategoryView') }}">
        Categories
    </a>
</nav>
<div class="container">
        <h1>Crear Invice</h1>
        <form action="{{ route('invoices-create') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="client_id">Cliente</label>
                <select name="client_id" id="client_id" class="form-control">
                    @foreach($clients as $client)
                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="products">Productos</label>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>
                                <input type="checkbox" name="products[{{ $loop->index }}][id]" value="{{ $product->id }}">
                                {{ $product->name }}
                            </td>
                            <td>
                                <input type="number" name="products[{{ $loop->index }}][quantity]" min="1" class="form-control">
                            </td>
                            <td>
                                <input type="number" name="products[{{ $loop->index }}][price]" min="0" step="0.01" class="form-control" value="{{ $product->price }}">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <button type="submit" class="btn btn-primary">Crear Orden</button>
        </form>

        <h2 class="mt-5">Invoices Existentes</h2>
        <table class="table">
            <thead>
            <tr>
                <th>ID de la Orden</th>
                <th>Cliente</th>
                <th>Fecha</th>
                <th>Productos</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->client->name }}</td>
                    <td>{{ $order->order_date }}</td>
                    <td>
                        <ul>
                            @foreach($order->products as $product)
                                <li>{{ $product->name }} ({{ $product->pivot->quantity }} x ${{ $product->pivot->price }})</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
</div>
