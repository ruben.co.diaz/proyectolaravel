<div>
    <div>
        <nav style="text-align: center; background-color: #ADD8E6;">
            <a href="{{ route('inici') }}">
                Home
            </a>
            <a href="{{ route('productsView') }}">
                Products
            </a>
            <a href="{{ route('clientsView') }}">
                Clients
            </a>
            <a href="{{ route('invoicesView') }}">
                Invoices
            </a>
            <a href="{{ route('CategoryView') }}">
                Categories
            </a>
        </nav>
    </div>
    <div>
        <h1>Categories: </h1>
        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>IVA</th>
                <th>Esborrar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $categorie)
                <tr>
                    <td>{{$categorie->name}}</td>
                    <td>{{$categorie->percent}}</td>
                    <td><button type="button" onclick="location.href='{{url('/categories/'.$categorie->id.'/delete')}}'">Delete</button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <button type="button" onclick="location.href='{{url('/formCategories')}}'">Afegir categories</button>
    </div>
</div>
