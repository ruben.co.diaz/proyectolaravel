<link rel="stylesheet" type="text/css" href="{{ url('/resources/css/menu.css') }}" />
<nav style="text-align: center; background-color: #ADD8E6;">
    <a href="{{ route('inici') }}">
        Home
    </a>
    <a href="{{ route('productsView') }}">
        Products
    </a>
    <a href="{{ route('clientsView') }}">
        Clients
    </a>
    <a href="{{ route('invoicesView') }}">
        Invoices
    </a>
    <a href="{{ route('CategoryView') }}">
        Categories
    </a>
</nav>
<div>
    <form class="form-horizontal" method="POST" action="{{ route('create') }}">
        @csrf
        <label for="name">Nom:</label><br>
        <input type="text" id="name" name="name"><br>
        <label for="description">Descripció:</label><br>
        <input type="text" id="description" name="description"><br>
        <label for="price">Preu:</label><br>
        <input type="number" id="price" name="price"><br>
        <label for="quantity">Quantitat:</label><br>
        <input type="number" id="quantity" name="quantity"><br>
        <button type="submit" class="btn btn-primary">Crear</button>
    </form>
    <div>
        <button onclick="location.href='{{ url('/products') }}'">Tornar</button>
    </div>
</div>
