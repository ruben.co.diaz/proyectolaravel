<link rel="stylesheet" type="text/css" href="{{ url('/resources/css/menu.css') }}" />

<nav style="text-align: center; background-color: #ADD8E6;">
    <a href="{{ route('inici') }}">
        Home
    </a>
    <a href="{{ route('productsView') }}">
        Products
    </a>
    <a href="{{ route('clientsView') }}">
        Clients
    </a>
    <a href="{{ route('invoicesView') }}">
        Invoices
    </a>
    <a href="{{ route('CategoryView') }}">
        Categories
    </a>
</nav>

<div>
    <form class="form-horizontal" method="POST" action="{{ route('clients-create') }}">
        @csrf
        <label for="name">Nom:</label>
        <input type="text" id="name" name="name">
        <label for="age">Edat:</label>
        <input type="text" id="age" name="age">
        <label for="address">Adreça:</label>
        <input type="text" id="address" name="address">
        <label for="city">Ciutat:</label>
        <input type="text" id="city" name="city">
        <label for="country">País:</label>
        <input type="text" id="country" name="country"><br>
        <button type="submit" class="btn btn-primary">Crear</button>
    </form>
    <div>
        <button onclick="location.href='{{ url('/clients') }}'">Tornar</button>
    </div>
</div>
