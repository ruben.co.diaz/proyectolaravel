<div>

    <p>Segur que vols borrar el client {{ $client->name }}?</p>

    <button onclick="location.href='{{ url('/client/'.$client->id.'/delete') }}'">Si</button>
    <button onclick="location.href='{{ url('/clients') }}'">No</button>
</div>
