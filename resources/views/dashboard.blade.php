<x-app-layout>
    <nav style="text-align: center; background-color: #ADD8E6;">
        <a href="{{ route('inici') }}">
            Home
        </a>
        <a href="{{ route('productsView') }}">
            Products
        </a>
        <a href="{{ route('clientsView') }}">
            Clients
        </a>
        <a href="{{ route('invoicesView') }}">
            Invoices
        </a>
        <a href="{{ route('CategoryView') }}">
            Categories
        </a>
    </nav>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
